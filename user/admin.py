from django.contrib import admin
from user.models import Borrower,Student,Staff

# Register your models here.
@admin.register(Borrower)
class BorrowerAdmin(admin.ModelAdmin):
    list_display=('borrower_id','accession_number','author_of_book','title_of_book')
#admin.site.register(Borrower,BorrowerAdmin)

@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display=('student_id','student_name','student_borrowbook')
#admin.site.register(Student,StudentAdmin)

@admin.register(Staff)
class StaffAdmin(admin.ModelAdmin):
    list_display=('staff_id','staff_name','designation')
#admin.site.register(Staff,StaffAdmin)
