from django.db import models

# Create your models here.
class Borrower(models.Model):
    borrower_id=models.IntegerField()
    accession_number=models.IntegerField()
    author_of_book=models.CharField(max_length=70)
    title_of_book=models.CharField(max_length=70)

class Student(models.Model):
    student_id=models.IntegerField()
    student_name=models.CharField(max_length=70)
    student_borrowbook=models.CharField(max_length=70)

class Staff(models.Model):
    staff_id=models.IntegerField()
    staff_name=models.CharField(max_length=70)
    designation=models.CharField(max_length=70)