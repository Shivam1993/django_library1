from django.contrib import admin
from books.models import Book,Binding,Category,Shelf

# Register your models here.
@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    list_display=('book_Code','publication_year','book_title','language')
#admin.site.register(Book,BookAdmin)    

@admin.register(Binding)
class BindingAdmin(admin.ModelAdmin):
    list_display=('binding_id','binding_name')
#admin.site.register(Binding,BindingAdmin)

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display=('category_id','category_name')
#admin.site.register(Category,CategoryAdmin)

@admin.register(Shelf)
class ShelfAdmin(admin.ModelAdmin):
    list_display=('shelf_id','shelf_no','floor_no')
#admin.site.register(Shelf,ShelfAdmin)



