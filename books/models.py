from django.db import models

# Create your models here.
class Book(models.Model):
    book_Code=models.IntegerField()
    publication_year=models.IntegerField()
    book_title=models.CharField(max_length=70)
    language=models.CharField(max_length=70)

class Binding(models.Model):
    binding_id=models.IntegerField()
    binding_name=models.CharField(max_length=70)

class Category(models.Model):
    category_id=models.IntegerField()
    category_name=models.CharField(max_length=70)

class Shelf(models.Model):
    shelf_id=models.IntegerField()
    shelf_no=models.IntegerField()
    floor_no=models.IntegerField()
    


    